import React from "react";
// import JSONInput from 'react-json-editor-ajrm'
// import locale from 'react-json-editor-ajrm/locale/en';
import { connect } from "@cerebral/react";
import { state, signal, props } from "cerebral/tags";
import Line from "../Line";

export default connect({
    confObj: state`conf.obj`,
    confStr: state`conf.str`,
    updateConf: signal`conf.updateConf`,
    resetJson: signal`conf.loadConf`,
}, class OutputPanel extends React.Component {

    resetJson() {
        this.props.resetJson()
    }

    saveJson() {
        this.props.updateConf({data: this.json})
    }

    onJsonInpChange(e) {
        this.json = e.target.value
    }

    // render() {
    //     this.json = this.props.confStr
    //     return <div className={this.props.className}>
    //     <button onClick={e => this.saveJson()}>write to file</button>
    //     <button onClick={e => this.resetJson()}>reset</button>
    //     <JSONInput
    //         onChange={this.onJsonInpChange.bind(this)}
    //         id='a_unique_id'
    //         locale={ locale }
    //         placeholder={this.props.confObj}
    //         width='100%'
    //         theme='light_mitsuketa_tribute'
    //     /></div>
    // }

    render() {
        this.json = this.props.confStr

        return <div className={this.props.className}>
            <button onClick={e => this.saveJson()}>write to file</button>
            <button onClick={e => this.resetJson()}>reset</button>
            <textarea defaultValue={this.props.confStr} onChange={this.onJsonInpChange.bind(this)} />
        </div>
    }
});

